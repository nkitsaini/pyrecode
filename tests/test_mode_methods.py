from recode.models import Position, BufferChange

def test_position():
	buffer = "hii\nbye"
	### START
	start_position = Position(line=1, char=1)
	assert start_position.to_index(buffer) == 0
	assert Position.from_index(buffer, 0) == start_position


	### END
	index = len(buffer) - 1
	end_position = Position.from_index(buffer, index)
	assert end_position.to_index(buffer) == index
	assert Position(line=2, char=3).to_index(buffer) == index


def test_buffer_change():
	buf1 = "Hello everyone"
	buf2 = "How are you, are you one"

	change12 = BufferChange.from_buffers(buf1, buf2)
	assert change12.apply(buf1) == buf2

	change21 = BufferChange.from_buffers(buf2, buf1)
	assert change21.apply(buf2) == buf1

	change01 = BufferChange.from_buffers("", buf1)
	assert change01.apply("") == buf1


	change10 = BufferChange.from_buffers(buf1, "")
	assert change10.apply(buf1) == ""

	change11 = BufferChange.from_buffers(buf1, buf1)
	assert change11.apply(buf1) == buf1

def test_buffer_change_with_newline():
	buffer_state = [
		"hii",
		"hii",
		"hii\nbye",
		"hii\nbye",
		"hii\n\nbye",
		"hii\na\nbye",
		"hii\na\nbye"
	]
	changes = [
		"",
		"\nbye",
		"",
		"\n",
		"a",
		"",
		"",
	]
	for i, (a, b) in enumerate(zip(buffer_state, buffer_state[1:])):
		change = BufferChange.from_buffers(a, b)
		assert change.apply(a) == b
		assert change.new_buffer == changes[i], i
