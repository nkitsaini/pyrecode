from recode.checker import basic_check
import recode.models as m
from dataclasses import asdict
import json
from pathlib import Path


SAMPLES_DIR = Path("samples/")
def test_basic_check():
	assert basic_check("{}") == False

	for correct_file in SAMPLES_DIR.iterdir():
		if not correct_file.is_file():
			continue
		with open(correct_file) as f:
			assert basic_check(f.read())

	for wrong_file in (SAMPLES_DIR/'invalid').iterdir():
		if not wrong_file.is_file():
			continue
		with open(wrong_file) as f:
			assert not basic_check(f.read())

