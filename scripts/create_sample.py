import arrow
import pyrecod.models as m
import dataclasses
import jsonlines
from pprint import pprint

def to_ms(sec: float) -> int:
	return int(sec*1000)

SECOND__MS = 1000
now: arrow.Arrow = arrow.now()

last__ms: int = to_ms(now.float_timestamp)
last_buffer = ""
states = []

for i in range(10):
	curr_time = last__ms + to_ms(0.2)
	curr_buffer = "hello" + "".join([str(ci) for ci in range(i)])
	cursor_position = m.Position.from_index(curr_buffer, len(curr_buffer) - 1)
	visual_selection = None
	state = m.EditorState(
			cursor_position=cursor_position,
			buffer_change=m.BufferChange.from_buffers(last_buffer, curr_buffer),
			visual_selection=visual_selection
	)
	snapshot = m.SnapShot(timestamp__ms=curr_time, state=state)
	states.append(dataclasses.asdict(snapshot))

	last_buffer = curr_buffer
	last__ms = curr_time

with open("sample.jsonl", "w") as f:
	with jsonlines.Writer(f) as jsonl_f:
		for state in states:
			jsonl_f.write(state)


pprint(states)
