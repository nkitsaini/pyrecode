import dacite
import json
from .models import SnapShot
import typing as t

def check_ss(ss: t.Dict) -> bool:
	try:
		dacite.from_dict(data_class=SnapShot, data=ss)
		return True
	except dacite.DaciteError:
		return False

def basic_check(content: str) -> bool:
	try:
		for l in content.splitlines():
			if not check_ss(json.loads(l)):
				return False
		return True
	except json.JSONDecodeError:
		return False
