class RecodException(Exception):
	pass

class OutOfBound(RecodException):
	pass

class Unreachable(RecodException):
	pass

