import typing as t
from . import exceptions as exc
from dataclasses import dataclass

Buffer = str

@dataclass
class BufferChange:
	"""
	buffer change from 0 to i32.max() will always replace all of buffer
	end_index is exclusive while start_index is inclusive
	"""
	old_start_index: int
	old_end_index: int
	new_buffer: str

	@classmethod
	def from_buffers(cls, old_buffer: Buffer, new_buffer: Buffer):
		start_index = 0
		end_index = len(old_buffer)
		for a, b in zip(old_buffer, new_buffer):
			if a==b:
				start_index += 1
			else:
				break
		for a, b in zip(reversed(old_buffer[start_index:]), reversed(new_buffer[start_index:])):
			if a==b:
				end_index -=1
			else:
				break
		new_end_index = len(new_buffer) - (len(old_buffer) - end_index)
		return cls(old_start_index=start_index, old_end_index=end_index, new_buffer=new_buffer[start_index:new_end_index])


	def apply(self, buffer: Buffer) -> Buffer:
		if self.old_start_index > self.old_end_index:
			return buffer

		new_buffer = buffer[:self.old_start_index] + self.new_buffer + buffer[self.old_end_index:]
		return new_buffer


@dataclass
class Position:
	line: int
	char: int

	@classmethod
	def from_index(cls, buffer: str, index: int):
		# TODO: for negative indexes
		if  index >= len(buffer) or index < 0:
			raise exc.OutOfBound("Index not inside buffer index")

		# TODO: for windows
		lines = buffer.split("\n")

		for i, line in enumerate(lines):
			if len(line) >= index:
				return cls(line=i + 1, char=index+1)
			else:
				index -= len(line)
				index -= 1 # for \n that we'd dropped

		raise exc.Unreachable



	def to_index(self, buffer: str) -> int:
		line_index = self.line - 1
		character_index = self.char - 1
		lines = buffer.split("\n")
		ri = 0
		for line in lines[:line_index]:
			ri += len(line) + 1  # +1 for \n
		return ri + character_index


@dataclass
class EditorState:
	cursor_position: Position
	buffer_change: BufferChange
	visual_selection: t.Optional[t.Tuple[Position, Position]] = None

@dataclass
class SnapShot:
	timestamp__ms: int
	state: EditorState
